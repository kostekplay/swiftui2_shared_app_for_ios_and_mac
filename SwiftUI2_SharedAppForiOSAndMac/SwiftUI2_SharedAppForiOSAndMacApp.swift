////  SwiftUI2_SharedAppForiOSAndMacApp.swift
//  SwiftUI2_SharedAppForiOSAndMac
//
//  Created on 01/03/2021.
//  
//

import SwiftUI

@main
struct SwiftUI2_SharedAppForiOSAndMacApp: App {
    var body: some Scene {
        WindowGroup {
            HomeView()
                .preferredColorScheme(.light)
        }
    }
}

#if !os(iOS)

extension NSTextField {
    open override var focusRingType: NSFocusRingType{
        get{.none}
        set{}
    }
}

#endif
