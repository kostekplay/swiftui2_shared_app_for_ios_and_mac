////  ContentView.swift
//  SwiftUI2_SharedAppForiOSAndMac
//
//  Created on 01/03/2021.
//  
//

import SwiftUI

struct HomeView: View {
    
    @StateObject var homeData = LoginViewModel()
    
    var body: some View {
        
        VStack {
            HStack {
                Text("Fitness You\nWanna Have")
                    .font(.title)
                    .fontWeight(/*@START_MENU_TOKEN@*/.bold/*@END_MENU_TOKEN@*/)
                    .foregroundColor(.black)
                    .padding(.leading, 25)
                Spacer()
            }
            .padding(
            )
            .overlay(
                HStack{
                    Image("cloud")
                        .shadow(color: Color/*@START_MENU_TOKEN@*/.black/*@END_MENU_TOKEN@*/.opacity(0.2), radius: 5, x: 2, y: 5)
                        .offset(x: -85, y: 20)
                    
                    Spacer()
                    VStack{
                        Image("cloud")
                            .shadow(color: Color/*@START_MENU_TOKEN@*/.black/*@END_MENU_TOKEN@*/.opacity(0.2), radius: 5, x: 2, y: 5)
                            .offset(x: 30)
                        Spacer()
                    }
                }, alignment: .bottomLeading
            )
            
            Image("logo")
                .resizable()
                .scaledToFit()
                .padding()
                .padding(.horizontal)
            
            // Login / Register
            
            ZStack{
                
                if homeData.gotoRegister {
                    
                    RegisterView()
                        .transition(.scale)
                    
                } else {
                    
                    LoginView()
                        .transition(.scale)
                }
                
            }
            
            .overlay(
                Button(action: /*@START_MENU_TOKEN@*/{}/*@END_MENU_TOKEN@*/, label: {
                    Image("right")
                        .renderingMode(.template)
                        .resizable()
                        .modifier(LoginButtonModifier())
                })
                .buttonStyle(PlainButtonStyle())
                .offset(x: -65)
                , alignment: .bottomTrailing
            )
            
        }
        .frame(maxHeight: .infinity)
        .background(Color("bg").ignoresSafeArea(.all,edges: .all))
        .environmentObject(homeData)
    }
}

struct HomeView_Previews: PreviewProvider {
    static var previews: some View {
        HomeView()
    }
}
