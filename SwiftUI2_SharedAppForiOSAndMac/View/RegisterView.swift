////  RegisterView.swift
//  SwiftUI2_SharedAppForiOSAndMac
//
//  Created on 03/03/2021.
//  
//

import SwiftUI

struct RegisterView: View {
    
    @EnvironmentObject var homeData: LoginViewModel
    
    var body: some View {
        
        
        VStack(alignment: .leading, spacing: 18, content: {
            
            Label(
                title: {
                    Text("Pleace Register")
                        .font(.title2)
                        .fontWeight(.bold)
                        .foregroundColor(.black) },
                icon: {
                    Button(action: {
                        withAnimation {
                            homeData.gotoRegister.toggle()
                        }
                    }, label: {
                        Image("right")
                            .resizable()
                            .renderingMode(/*@START_MENU_TOKEN@*/.template/*@END_MENU_TOKEN@*/)
                            .frame(width: 20, height: 20)
                            .scaledToFit()
                            .foregroundColor(.black)
                            .rotationEffect(.init(degrees: 180))
                    })
                    .buttonStyle(PlainButtonStyle())
                })
            
            Label(
                title: { TextField("Enter Email", text: $homeData.email)
                    .textFieldStyle(PlainTextFieldStyle())
                },
                icon: { Image(systemName: "envelope")
                    .frame(width: 30)
                }
            )
            .foregroundColor(.gray)
            
            Divider()
            
            Label(
                title: { TextField("Password", text: $homeData.password)
                    .textFieldStyle(PlainTextFieldStyle())
                },
                icon: { Image(systemName: "lock")
                    .frame(width: 30)
                }
            )
            .foregroundColor(.gray)
            
            Divider()
            
            Label(
                title: { TextField("Re-Enter Password", text: $homeData.reEnter)
                    .textFieldStyle(PlainTextFieldStyle())
                },
                icon: { Image(systemName: "lock")
                    .frame(width: 30)
                }
            )
            .foregroundColor(.gray)
            
        })
        .modifier(LoginViewModifier())
    }
}

struct RegisterView_Previews: PreviewProvider {
    static var previews: some View {
        RegisterView()
    }
}
