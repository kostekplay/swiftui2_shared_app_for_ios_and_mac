////  SwiftUIView.swift
//  SwiftUI2_SharedAppForiOSAndMac
//
//  Created on 03/03/2021.
//  
//

import SwiftUI

struct LoginViewModifier: ViewModifier {

    func body(content: Content) -> some View {
        
        return content
        
            .padding()
            .padding(.bottom)
            .background(Color.white)
            .cornerRadius(24)
            .padding(.vertical)
            .padding(.bottom, 10)
            .padding(.horizontal, 25)
        
    }
    
}
