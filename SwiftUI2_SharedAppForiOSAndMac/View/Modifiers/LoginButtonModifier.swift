////  LoginButtonModifier.swift
//  SwiftUI2_SharedAppForiOSAndMac
//
//  Created on 03/03/2021.
//  
//

import SwiftUI

struct LoginButtonModifier: ViewModifier {

    func body(content: Content) -> some View {
        
        return content
        
            .scaledToFit()
            .frame(width: 20, height: 20)
            .foregroundColor(Color.white)
            .padding(12)
            .background(
                LinearGradient(gradient: Gradient(colors: [Color("gradient1"), Color("gradient2")]), startPoint: .topLeading, endPoint: .bottomTrailing)
            )
            .clipShape(Circle())

    }

}
