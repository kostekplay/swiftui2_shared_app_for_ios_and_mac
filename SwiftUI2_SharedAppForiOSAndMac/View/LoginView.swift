////  LoginView.swift
//  SwiftUI2_SharedAppForiOSAndMac
//
//  Created on 03/03/2021.
//  
//

import SwiftUI

struct LoginView: View {
    
    @EnvironmentObject var homeData: LoginViewModel
    
    var body: some View {
        
        
        VStack(alignment: .leading, spacing: 18, content: {
            
            Text("Pleace Login")
                .font(.title2)
                .fontWeight(.bold)
                .foregroundColor(.black)
            
            Label(
                title: { TextField("Enter Email", text: $homeData.email)
                    .textFieldStyle(PlainTextFieldStyle())
                },
                icon: { Image(systemName: "envelope")
                    .frame(width: 30)
                }
            )
            .foregroundColor(.gray)
            
            Divider()
            
            Label(
                title: { TextField("Password", text: $homeData.password)
                    .textFieldStyle(PlainTextFieldStyle())
                },
                icon: { Image(systemName: "lock")
                    .frame(width: 30)
                }
            )
            .foregroundColor(.gray)
            
            Divider()
            
            HStack {
                
                Button(action: {
                }, label: {
                    Text("Forgot Details?")
                        .font(.caption)
                        .fontWeight(.bold)
                })
                .buttonStyle(PlainButtonStyle())
                
                Spacer()
                
                Button(action: {
                    withAnimation {
                        homeData.gotoRegister.toggle()
                    }
                }, label: {
                    Text("Create Accounts.")
                        .font(.caption)
                        .fontWeight(.bold)
                })
                .buttonStyle(PlainButtonStyle())
            }
            .foregroundColor(.gray)
            
        })
        .modifier(LoginViewModifier())
        
    }
}

struct LoginView_Previews: PreviewProvider {
    static var previews: some View {
        LoginView()
    }
}
